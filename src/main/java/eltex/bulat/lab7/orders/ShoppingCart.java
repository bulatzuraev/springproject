package eltex.bulat.lab7.orders;



import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import eltex.bulat.lab7.product.AbstractProduct;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public class ShoppingCart <T extends AbstractProduct> extends ArrayList<T> implements Serializable {
    private HashSet<UUID> uuidSet = new HashSet<UUID>();
    private UUID id;
    public HashSet<UUID> getUuidSet() {
        return uuidSet;
    }
    @JsonProperty("id")
    public UUID getId() {
        return id;
    }
    @JsonProperty("items")
    public Object[] items() {
        return this.toArray(new Object[size()]);
    }

    public ShoppingCart(){
        super();

        id = UUID.randomUUID();
    }
    @Override
    public boolean add(T product){
        uuidSet.add(product.getObjectId());
        return super.add(product);

    }
    public void read(){
        System.out.println(this);
    }
    public T search(UUID id){
        for (T product : this){
            if (product.getObjectId().equals(id)){
                return product;
            }
        }
        return null;
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for (T product : this){
            sb.append(product);
        }
        return sb.toString();
    }
}
