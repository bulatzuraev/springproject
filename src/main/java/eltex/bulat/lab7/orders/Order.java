package eltex.bulat.lab7.orders;

import java.io.Serializable;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.UUID;


public class Order implements Serializable {
    private UUID id;
    private static HashMap<Instant, Order> orderMap = new HashMap<Instant, Order>();
    private Status status;
    private Instant creationTime;
    private Duration awaitingTime;
    public ShoppingCart shoppingCart;
    public Credential credential;

    public UUID getId() {
        return id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Instant getCreationTime() {
        return creationTime;
    }

    public Duration getAwaitingTime() {
        return awaitingTime;
    }

    public void setAwaitingTime(Duration awaitingTime) {
        this.awaitingTime = awaitingTime;
    }

    public static HashMap<Instant, Order> getOrderMap() {
        return orderMap;
    }

    public Order(int awaitingTime, ShoppingCart shoppingCart, Credential credential){
        id = UUID.randomUUID();
        this.status = Status.awaiting;
        this.creationTime = Instant.now();
        this.awaitingTime = Duration.ofSeconds(awaitingTime);
        this.shoppingCart = shoppingCart;
        this.credential = credential;
        orderMap.put(creationTime, this);
    }
    public void read(){
        System.out.println(this);
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("ID: ");
        sb.append(id);
        sb.append(System.getProperty("line.separator"));
        sb.append("Status: ");
        sb.append(status);
        sb.append(System.getProperty("line.separator"));
        sb.append("Creation time: ");
        sb.append(creationTime);
        sb.append(System.getProperty("line.separator"));
        sb.append("Awaiting time: ");
        sb.append(awaitingTime);
        sb.append(System.getProperty("line.separator"));
        return sb.toString() + shoppingCart.toString() + credential.toString();
    }
    public void changeStatus(){
        status = Status.processed;
    }
}
