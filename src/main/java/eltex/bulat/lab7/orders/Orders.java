package eltex.bulat.lab7.orders;

import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Iterator;
import java.util.LinkedList;
@Component
public class Orders <T extends Order> extends LinkedList<T> {
    public void makePurchase(T order){
        this.add(order);
    }
    public void checkUp(){
        Iterator<T> it = this.iterator();
        while (it.hasNext()){
            T order = it.next();
            Instant awaiting = order.getCreationTime().plus(order.getAwaitingTime());
            if (order.getStatus() == Status.processed || Instant.now().compareTo(awaiting) > 0){
                it.remove();
            }
        }
    }
    public void read(){
        System.out.println(this);
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for (T order : this){
            sb.append(order);
        }
        return sb.toString();
    }
}
