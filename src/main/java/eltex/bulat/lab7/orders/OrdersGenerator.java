package eltex.bulat.lab7.orders;

import eltex.bulat.lab7.orders.Credential;
import eltex.bulat.lab7.orders.Order;
import eltex.bulat.lab7.orders.Orders;
import eltex.bulat.lab7.orders.ShoppingCart;
import eltex.bulat.lab7.product.AbstractProduct;
import eltex.bulat.lab7.product.Coffee;
import eltex.bulat.lab7.product.Tea;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class OrdersGenerator implements Runnable {
    Orders orders;
    int sleepTime;
    public OrdersGenerator(Orders orders){
        this.orders = orders;
        sleepTime = 2;
    }
    public OrdersGenerator(Orders orders, int sleepTime){
        this.orders = orders;
        this.sleepTime = sleepTime;
    }
    public static AbstractProduct generateProduct(String product){
        if (product == "Tea"){
            return new Tea();
        } else if (product == "Coffee"){
            return new Coffee();
        }
        return null;
    }
    private static ShoppingCart generateShoppingCart(){
        String[] productType = {"Tea","Coffee"};
        ShoppingCart cart = new ShoppingCart();
        Random rand = new Random();
        for (int i = 0; i < 5; ++i){
            cart.add(generateProduct(productType[rand.nextInt(2)]));
        }
        return cart;
    }
    private static Credential generateCredential(){
        Random rand = new Random();
        int i = rand.nextInt(100);
        String firstName = "firstName" + i;
        String secondName = "firstName" + i;
        String middleName = "firstName" + i;
        return new Credential(firstName, secondName, middleName);
    }
    public static Order generateOrder(){
        Random rand = new Random();
        return new Order(rand.nextInt(10),generateShoppingCart(),generateCredential());
    }
    public void run(){
        Random rand = new Random();
        while (true){
            try {
                synchronized (orders){
                    orders.makePurchase(new Order(rand.nextInt(10),generateShoppingCart(),generateCredential()));
                    System.out.println("makePurchase, sleeptime="+sleepTime);
                }
                TimeUnit.SECONDS.sleep(sleepTime);
            } catch (InterruptedException e){
                System.err.println("Interrupted");
                break;
            }

        }
    }
}
