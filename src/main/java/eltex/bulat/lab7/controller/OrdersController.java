package eltex.bulat.lab7.controller;


import eltex.bulat.lab7.manager.AManageOrder;
import eltex.bulat.lab7.manager.IOrder;
import eltex.bulat.lab7.manager.ManagerOrderJSON;
import eltex.bulat.lab7.orders.Order;
import eltex.bulat.lab7.orders.Orders;
import eltex.bulat.lab7.orders.OrdersGenerator;
import eltex.bulat.lab7.orders.ShoppingCart;
import eltex.bulat.lab7.product.AbstractProduct;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/")
public class OrdersController {
    @Autowired
    private Service service;
    private static final Logger logger = LogManager.getLogger(OrdersController.class);
    @RequestMapping(method=RequestMethod.GET)
    public Object commands(
            @RequestParam(value="command") String command,
            @RequestParam(required=false,value="order_id",defaultValue = "null") String orderId,
            @RequestParam(required=false,value="cart_id",defaultValue = "null") String cartId
    ){
        if ("readAll".equals(command)){
            logger.info("reading orders");
            return service.getAllOrders();
        }
        if ("readById".equals(command)&&!"null".equals(orderId)){
            logger.info("reading order");
            return service.getById(orderId);
        }
        if("addToCart".equals(command)&&!"null".equals(cartId)){
            logger.info("adding to cart");
            return service.addToCart(cartId);
        }
        if ("delById".equals(command)&&!"null".equals(orderId)){
            logger.info("deleting");
            return service.delById(orderId);
        }
        return 3;
    }
}
@Component
class Service{
    private ManagerOrderJSON manager;
    private Orders orders;
    @Autowired
    public Service(ManagerOrderJSON manager, Orders orders){
        this.manager = manager;
        this.orders = orders;
        manager.readAll();
    }
    public Orders getAllOrders(){
        return orders;
    }
    public Order getById(String id){
        Order order = null;
        Iterator<Order> it = orders.iterator();
        while (it.hasNext()){
            Order temp = it.next();
            if (temp.getId().equals(UUID.fromString(id))){
                order = temp;
            }
        }
        return order;
    }
    public UUID addToCart(String id){
        Iterator<Order> orderIt = orders.iterator();
        UUID productId = null;
        while (orderIt.hasNext()){
            Order temp = orderIt.next();
            if (temp.shoppingCart.getId().equals(UUID.fromString(id))){
                AbstractProduct product = OrdersGenerator.generateProduct("Tea");
                temp.shoppingCart.add(product);
                productId = product.getObjectId();
                manager.saveAll();
            }
        }
        return productId;
    }
    public int delById(String id){
        Iterator<Order> orderIt = orders.iterator();
        UUID uuid = null;
        try{
            uuid = UUID.fromString(id);
        }catch(IllegalArgumentException e){
            return 1;
        }
        while (orderIt.hasNext()){
            if (orderIt.next().getId().equals(uuid)){
                orderIt.remove();
                if(!manager.saveAll()){
                    return 2;
                }
                return 0;
            }
        }
        return 1;
    }
}