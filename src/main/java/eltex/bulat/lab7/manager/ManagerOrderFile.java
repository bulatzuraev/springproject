package eltex.bulat.lab7.manager;

import eltex.bulat.lab7.orders.Order;
import eltex.bulat.lab7.orders.Orders;

import java.io.*;
import java.util.Iterator;
import java.util.UUID;

public class ManagerOrderFile extends AManageOrder {
    String fileName;
    public ManagerOrderFile(Orders orders, String fileName){
        super(orders);
        File file = new File(fileName);
        try{
            file.createNewFile();
        } catch(IOException e){

        }
        this.fileName = fileName;
    }
    private static Order searchById(Orders orders, UUID id){
        Iterator<Order> it = orders.iterator();
        Order order = null;
        while (it.hasNext()){
            Order temp = it.next();
            if (temp.getId().equals(id)){
                order = temp;
            }
        }
        return order;
    }

    private Orders makeList(){
        Object obj = null;
        Orders orders = null;
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName))) {
            obj = in.readObject();
            orders = (Orders)obj;
            System.out.println(orders);
        } catch (FileNotFoundException e){
            System.out.println("file not found");
        } catch (IOException e){
            e.printStackTrace();
        } catch (ClassNotFoundException e){
            System.out.println("Class not found");
        }
        return orders;
    }
    private boolean saveList(Orders orders){
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName,false))) {
            out.writeObject(orders);
        } catch (FileNotFoundException e){
            System.out.println("file not found");
            return false;
        } catch (IOException e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void readById(UUID id) {
        Order order = searchById(orders, id);
        Order orderInput = searchById(makeList(), id);
        order = orderInput;
    }

    public void readAll(){
        this.orders.clear();
        this.orders.addAll(makeList());
    }
    public boolean saveAll(){
        return saveList(orders);
    }
    public boolean saveById(UUID id){
        Order order = searchById(this.orders, id);
        Orders orders = makeList();
        Order orderTemp = searchById(orders, id);
        if (orderTemp == null){
            orders.add(order);
        } else {
            orderTemp = order;
        }
        return saveList(orders);
    }

}
