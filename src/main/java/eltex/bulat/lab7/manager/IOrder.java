package eltex.bulat.lab7.manager;

import java.io.IOException;
import java.util.UUID;

public interface IOrder {
    void readById(UUID id);
    boolean saveById(UUID id);
    void readAll();
    boolean saveAll();
}
