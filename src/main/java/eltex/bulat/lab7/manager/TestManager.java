package eltex.bulat.lab7.manager;/*
package eltex.bulat.Task5;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

public class TestManager extends AManageOrder {
    String fileName;
    public TestManager(Orders orders, String fileName){
        super(orders);
        File file = new File(fileName);
        try{
            file.createNewFile();
        } catch(IOException e){

        }
        this.fileName = fileName;
    }
    private static Order searchById(Orders orders, UUID id){
        Iterator<Order> it = orders.iterator();
        Order order = null;
        while (it.hasNext()){
            Order temp = it.next();
            if (temp.getId().equals(id)){
                order = temp;
            }
        }
        return order;
    }

    private Orders makeList(){
        Orders orders = null;
        try (BufferedReader in = new BufferedReader(new FileReader(fileName))) {
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(ShoppingCart.class, new InterfaceAdapter())
                    .create();
            Type ordersType = new TypeToken<Orders<Order>>(){}.getType();
            orders = gson.fromJson(in.readLine(), ordersType);
            System.out.println(orders);
        } catch (FileNotFoundException e){
            System.out.println("file not found");
        } catch (IOException e){
            e.printStackTrace();
        }
        return orders;
    }
    private void saveList(Orders orders){
        try (BufferedWriter out = new BufferedWriter(new FileWriter(fileName,false))) {
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(ShoppingCart.class, new InterfaceAdapter())
                    .create();
            Type ordersType = new TypeToken<Orders<Order>>(){}.getType();
            out.write(gson.toJson(orders,ordersType));
        } catch (FileNotFoundException e){
            System.out.println("file not found");
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public void readById(UUID id) {
        Order order = searchById(orders, id);
        Order orderInput = searchById(makeList(), id);
        order = orderInput;
    }

    public void readAll(){
        this.orders.clear();
        this.orders.addAll(makeList());
    }
    public void saveAll(){
        saveList(orders);
    }
    public void saveById(UUID id){
        Order order = searchById(this.orders, id);
        Orders orders = makeList();
        Order orderTemp = searchById(orders, id);
        if (orderTemp == null){
            orders.add(order);
        } else {
            orderTemp = order;
        }
        saveList(orders);
    }

}
class InterfaceAdapter implements JsonSerializer<ShoppingCart>, JsonDeserializer<ShoppingCart>{

    private static final String CLASSNAME = "CLASSNAME";
    private static final String DATA = "DATA";

    public ShoppingCart deserialize(JsonElement jsonElement, Type type,
                         JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        ShoppingCart cart = new ShoppingCart();
        JsonArray jsonArray = jsonElement.getAsJsonArray();
        for (JsonElement element : jsonArray){
            JsonObject jsonObject = element.getAsJsonObject();
            JsonPrimitive prim = (JsonPrimitive) jsonObject.get(CLASSNAME);
            String className = prim.getAsString();
            Class klass = getObjectClass(className);
            cart.add(jsonDeserializationContext.deserialize(jsonObject.get(DATA), klass));
        }
        return cart;
    }
    public JsonElement serialize(ShoppingCart jsonElement, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonArray jsonArray = new JsonArray();
        Iterator<AbstractProduct> it = jsonElement.iterator();
        while (it.hasNext()){
            JsonObject jsonObject = new JsonObject();
            AbstractProduct product = it.next();
            jsonObject.addProperty("CLASSNAME", product.getClass().getName());
            jsonObject.add("DATA",jsonSerializationContext.serialize(product));
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }
    */
/****** Helper method to get the className of the object to be deserialized *****//*

    public Class getObjectClass(String className) {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            //e.printStackTrace();
            throw new JsonParseException(e.getMessage());
        }
    }
}
*/
