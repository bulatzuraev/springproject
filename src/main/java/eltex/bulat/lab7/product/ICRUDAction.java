package eltex.bulat.lab7.product;

public interface ICRUDAction {
    void create();
    void read();
    void update();
    void delete();
}
